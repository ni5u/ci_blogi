<?php

class Kayttaja_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function kirjaudu($data) {

        $condition = "tunnus =" . "'" . $data['tunnus'] . "' AND " . "salasana =" . "'" . $data['salasana'] . "'";
        $this->db->select('*');
        $this->db->from('kayttaja');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row()->id;
        } else {
            return 0;
        }
    }

    public function rekisteroi($data) {

        $this->db->insert('kayttaja', $data);
    }

    public function tarkistaEmail($email) {

        $this->db->select('*');
        $this->db->from('kayttaja');
        $this->db->where('email', $email);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function tarkistaTunnus($email) {

        $this->db->select('*');
        $this->db->from('kayttaja');
        $this->db->where('tunnus', $email);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

}
