<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Kirjoitus_model
 *
 * @author nisu
 */
class Kirjoitus_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function hae_kaikki() {
        $this->db->select('*, kirjoitus.id as id');
        $this->db->join('kayttaja', 'kayttaja.id = kirjoitus.kayttaja_id');
        $this->db->from('kirjoitus');
        
        $kysely = $this->db->get();
        return $kysely->result();
    }

    public function lisaa($data) {
        $this->db->insert("kirjoitus", $data);
        return $this->db->insert_id();
    }

    public function tallenna() {
        $data = array(
            'otsikko' => $this->input->post('otsikko'),
            'teksti' => $this->input->post('teksti'),
        );
    }
    
        public function hae($id) {
        $this->db->select('*, kirjoitus.id as id');
        $this->db->from('kirjoitus');
        $this->db->join('kayttaja', 'kayttaja.id = kirjoitus.kayttaja_id');
        $this->db->where('kirjoitus.id',$id);
        $kysely = $this->db->get();
        return $kysely->row();
    }
    
    public function poista($id) {
        $this->db->where('id', $id);
        $this->db->delete('kirjoitus');
    }

}
