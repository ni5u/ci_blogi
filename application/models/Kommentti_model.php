<?php

class Kommentti_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function hae($kirjoitus_id) {
        $this->db->where('kirjoitus_id', $kirjoitus_id);
        $this->db->join('kayttaja', 'kayttaja.id = kommentti.kayttaja_id');
        $kysely = $this->db->get("kommentti");
        return $kysely->result();
    }

    public function lisaa($data) {
        $this->db->insert("kommentti", $data);
        return $this->db->insert_id();
    }

    public function poista($id) {
        $this->db->select('kommentti.kirjoitus_id as kirjoitus_id');
        $this->db->where('kommentti_id', $id);
        $this->db->from('kommentti');
        $kysely = $this->db->get();        
        
        $this->db->where('kommentti_id', $id);
        $this->db->delete('kommentti');
        
        return $kysely->row()->kirjoitus_id;
    }
    
    public function poista_kaikki($kirjoitus_id) {
        $this->db->where('kirjoitus_id', $kirjoitus_id);
        $this->db->delete('kommentti');
    }

}
