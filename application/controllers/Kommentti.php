<?php

class Kommentti extends CI_Controller {
        public function __construct() {
        parent::__construct();
        $this->load->model("Kommentti_model");
    }

    public function tallenna() {
        
        $kirjoitus_id = $this->input->post('kirjoitus_id');
        $data = array(
            'teksti' => $this->input->post('teksti'),
            'kirjoitus_id' => $kirjoitus_id,
            'kayttaja_id' => $this->session->id,
        );

        $this->Kommentti_model->lisaa($data);

        redirect('kirjoitus/nayta/' . $kirjoitus_id, 'refresh');
    }
    
    public function poista($id) {
        $kirjoitus_id = $this->Kommentti_model->poista($id);
        redirect('kirjoitus/nayta/' . $kirjoitus_id, 'refresh');
    }
    /*
    public function poista_kaikki($kirjoitus_id) {
        $this->Kommentti_model->poista_kaikki($kirjoitus_id);
        redirect('kirjoitus/nayta/' . $kirjoitus_id, 'refresh');
    }
    */
}
