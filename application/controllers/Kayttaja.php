<?php

class Kayttaja extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function kirjautumistiedot() {
        $data['main_content'] = 'kayttaja/kirjaudu_view';
        $this->load->view("template", $data);
    }

    public function rekisteroitymistiedot() {
        $data['main_content'] = 'kayttaja/rekisteroidy_view';
        $this->load->view("template", $data);
    }

    public function kirjaudu() {
        $this->load->model('kayttaja_model');
        $tunnus = $this->input->post('tunnus');
        $data = array(
            'tunnus' => $tunnus,
            'salasana' => md5($this->input->post('salasana'))
        );
        $id = $this->kayttaja_model->kirjaudu($data);

        if ($id) {
            $logdata = ['id' => $id,
                'logged_in' => TRUE,
                'tunnus' => $tunnus,
            ];
            $this->session->set_userdata($logdata);
            redirect('kirjoitus/index', 'refresh');
        } else {
            $this->session->set_flashdata('virheilmoitus', 'Tunnus tai salasana väärin.');
            $data['main_content'] = 'kayttaja/kirjaudu_view';
            $this->load->view('template', $data);
        }
    }

    public function ulos() {        
        session_destroy();
        redirect('kirjoitus/index', 'refresh');
    }

    public function rekisteroidy() {
        $this->load->model('kayttaja_model');

        $data = array(
            'etunimi' => $this->input->post('etunimi'),
            'sukunimi' => $this->input->post('sukunimi'),
            'tunnus' => $this->input->post('tunnus'),
            'salasana' => md5($this->input->post('salasana')),
            'email' => $this->input->post('email'),
        );
        $tunnus_check = $this->kayttaja_model->tarkistaTunnus($data['tunnus']);
        $email_check = $this->kayttaja_model->tarkistaEmail($data['email']);

        if ($email_check && $tunnus_check) {
            $this->kayttaja_model->rekisteroi($data);
            $this->session->set_flashdata('onnistui_ilmoitus', 'Rekisteröinti onnistui. Voit kirjautua sisään.');
            redirect('kayttaja/kirjautumistiedot');
        } else {
            $this->session->set_flashdata('virheilmoitus', 'Tunnus tai sähköposti jo olemassa.');
            redirect('kayttaja/rekisteroitymistiedot');
        }
    }

}
