<?php

class Kirjoitus extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Kirjoitus_model');
    }

    public function index() {
        $data['kirjoitukset'] = $this->Kirjoitus_model->hae_kaikki();
        $data['main_content'] = 'kirjoitus/kirjoitukset_view';
        $this->load->view("template", $data);
    }

    public function lisaa() {
        $data['main_content'] = "kirjoitus/kirjoita_view";
        $this->load->view("template.php", $data);
    }

    public function tallenna() {
        $data = array(
            'otsikko' => $this->input->post('otsikko'),
            'teksti' => $this->input->post('teksti'),
            'kayttaja_id' => $this->input->post('id'),
        );

        $this->Kirjoitus_model->lisaa($data);

        redirect('kirjoitus/index', 'refresh');
    }

    public function nayta($id) {
        $this->load->model('Kommentti_model');

        $data['kommentit'] = $this->Kommentti_model->hae($id);
        $data['kirjoitus'] = $this->Kirjoitus_model->hae($id);

        $data['main_content'] = 'kirjoitus/kirjoitus_view';
        $this->load->view("template", $data);
    }
    
    public function poista($id) {
        $this->load->model('Kommentti_model');
        $this->Kommentti_model->poista_kaikki($id);
        $this->Kirjoitus_model->poista($id);
        redirect('kirjoitus/index', 'refresh');
    }

}
