<?php
echo form_open('kayttaja/kirjaudu');
?>

<?php
$onnistui_ilmoitus = $this->session->flashdata('onnistui_ilmoitus');
$virheilmoitus = $this->session->flashdata('virheilmoitus');

if ($onnistui_ilmoitus) {
    ?>

    <div class="alert alert-success">
        <?php echo $onnistui_ilmoitus; ?>
    </div>
<?php
}
if ($virheilmoitus) {
    ?>

    <div class="alert alert-danger">
    <?php echo $virheilmoitus; ?>
    </div>
<?php } ?>

<div class="form-group">
    <label>Tunnus:</label>
    <input type="text" name="tunnus" id="tunnus" maxlength="50">
</div>
<div class="form-group">
    <label>Salasana:</label>
    <input type="password" name="salasana" id="salasana" maxlength="30">
</div>
<?php
echo '<div class="buttons">';
echo form_submit('kirjaudu', 'Kirjaudu');
echo form_button('peruuta', 'Peruuta');
echo '</div>';
echo form_close();
