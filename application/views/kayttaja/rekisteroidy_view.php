

<?php
echo form_open('kayttaja/rekisteroidy');

$virheilmoitus = $this->session->flashdata('virheilmoitus');

if ($virheilmoitus) {
    ?>

    <div class="alert alert-danger">
        <?php echo $virheilmoitus; ?>
    </div>
<?php } ?>
<div class="form-group">
    <label>Etunimi:</label>
    <input type="text" name="etunimi" id="etunimi" maxlength="50">
</div>
<div class="form-group">
    <label>Sukunimi:</label>
    <input type="text" name="sukunimi" id="sukunimi" maxlength="50">
</div>
<div class="form-group">
    <label>Tunnus:</label>
    <input type="text" name="tunnus" id="tunnus" maxlength="50">
</div>
<div class="form-group">
    <label>Salasana:</label>
    <input type="password" name="salasana" id="salasana" maxlength="">
</div>
<div class="form-group">
    <label>Sähköposti:</label>
    <input type="email" name="email" id="email" maxlength="50">
</div>
<?php
echo '<div class="buttons">';
echo form_submit('rekisteroidy', 'Rekisteröidy');
echo form_button('peruuta', 'Peruuta');
echo '</div>';
echo form_close();
