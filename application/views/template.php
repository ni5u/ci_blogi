<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

        <title>Blogi</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url() . "assets/css/bootstrap.min.css" ?>" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url() . "assets/css/starter-template.css"; ?>" rel="stylesheet">

        <?php (isset($this->session->logged_in) ? $logged_in = TRUE : $logged_in = FALSE); ?> 
        <?php (isset($this->session->tunnus) ? $tunnus = $this->session->tunnus : ''); ?>  
        
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">Blogi</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">              
                        <li><?php print anchor(base_url(), "Etusivulle"); ?></li>
                        <li><?php ($logged_in ? print anchor("kirjoitus/lisaa", "Lisää kirjoitus") : print ""); ?></li>
                        <li><?php ($logged_in ? print anchor("kayttaja/ulos", "Kirjaudu ulos " . $tunnus) : print anchor("kayttaja/kirjautumistiedot", "Kirjaudu sisään")); ?></li>
                        <li><?php ($logged_in ? print "" : print anchor("kayttaja/rekisteroitymistiedot", "Rekisteröidy")); ?></li>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

            <div>
                <div class="col-lg-12">
                    <?php
                    $this->load->view($main_content);
                    ?>
                </div>
            </div>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url() . "assets/js/jquery.min.js"; ?>"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    </body></html>
