<?php
(isset($this->session->logged_in)?$logged_in = True:$logged_in = False);

print "<h2>" . $kirjoitus->otsikko . "</h2>";
print "<p>" . $this->pvm->format_sqldate_to_fin($kirjoitus->paivays) . " by " . $kirjoitus->tunnus . "</p>";
print "<p>" . $kirjoitus->teksti . "</p>";
if (isset($this->session->logged_in)) {
    echo form_open('kommentti/tallenna');
    $data = array(
        'name' => 'teksti',
        'id' => 'teksti',
        'value' => '',
        'rows' => '5',
        'cols' => '10',
        'style' => 'width:50%',
    );

    print "<p>Kirjoita kommentti:</p>";
    echo form_textarea($data);
    ?>
    <input type='hidden' name='kirjoitus_id' id='kirjoitus_id' value="<?php echo $kirjoitus->id ?>">
    <?php
    echo '<div class="buttons">';
    echo form_submit('talleta', 'Talleta');
    echo form_button('peruuta', 'Peruuta');
    echo '</div>';
    echo form_close();
}
print "<p><strong>Kommentit</strong></p>";

foreach ($kommentit as $kommentti) {
    print "<p>" . $this->pvm->format_sqldate_to_fin($kommentti->paivays) . " by " . $kommentti->tunnus . "</p>";
    print "<p>" . $kommentti->teksti . " <span>" . ($logged_in ? anchor('kommentti/poista/' . $kommentti->kommentti_id, 'Poista'):'') ."</span></p>";
    print "<hr>";
}
