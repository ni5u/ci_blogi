<?php
$id = $this->session->id;
?>
<h2>Kirjoituksen lisääminen</h2>
<?php
echo form_open('kirjoitus/tallenna');
?>

<input type="hidden" name="id" value="<?php echo $id; ?>" name="id">
<div class="form-group">
    <label>Otsikko:</label>
    <input name="otsikko" id="otsikko" maxlength="30">
</div>
<div class="form-group">
    <label>Teksti:</label>
    <?php
    $data = array(
        'name' => 'teksti',
        'id' => 'teksti',
        'value' => '',
        'rows' => '5',
        'cols' => '10',
        'style' => 'width:50%',
    );

    echo form_textarea($data);
    echo "</div>";
    echo '<div class="buttons">';
    echo form_submit('tallenna', 'Tallenna');
    echo form_button('peruuta', 'Peruuta');
    echo '</div>';
    echo form_close();
    