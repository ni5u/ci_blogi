<?php
(isset($this->session->logged_in)?$logged_in = True:$logged_in = False);

foreach ($kirjoitukset as $kirjoitus) {
    
    print "<p>" . $this->pvm->format_sqldate_to_fin($kirjoitus->paivays) . " by $kirjoitus->tunnus </p>\n";
    print "<p>" . anchor('kirjoitus/nayta/' . $kirjoitus->id, $kirjoitus->otsikko) . ($logged_in?anchor('kirjoitus/poista/' . $kirjoitus->id, ' [Poista kirjoitus]'):'') . "</p>\n";
    print "<hr>\n";
}